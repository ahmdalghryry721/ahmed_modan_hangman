import {
  homeState,
  // guessLetter,
  updateEmailInput,
  getGameState,
  setStateToLocalStorage,
  // submitEmailToCoursera,
  saveResponse,
  changeUser,
  removeCache,
  // homeReducer,
  CHANGE_USER,
  GET_GAME_STATE,
  SET_STATE_TO_LOCAL_STORAGE,
  UPDATE_EMAIL_INPUT,
  SAVE_RESPONSE,
  GUESS_LETTER,
  courseraAPI
} from './../src/redux/modules/homeReducer';
import { expect } from 'chai';
import sinon from 'sinon';

const mockStorage = function () {
  const storage = {};

  return {
    setItem(key, value) {
      storage[key] = value;
    },
    getItem(key) {
      return storage[key] || null;
    }
  };
};


describe('Constants(for Reducer)', () => {
  it('should describe themselves', () => {
    expect(GET_GAME_STATE).to.deep.equal('GET_GAME_STATE');
    expect(SET_STATE_TO_LOCAL_STORAGE).to.deep.equal('SET_STATE_TO_LOCAL_STORAGE');
    expect(UPDATE_EMAIL_INPUT).to.deep.equal('UPDATE_EMAIL_INPUT');
    expect(SAVE_RESPONSE).to.deep.equal('SAVE_RESPONSE');
    expect(GUESS_LETTER).to.deep.equal('GUESS_LETTER');
    expect(CHANGE_USER).to.deep.equal('CHANGE_USER');
    expect(courseraAPI).to.deep.equal('http://hangman.coursera.org/hangman/game');
  });
});


describe('Action Creators', () => {
  describe('updateEmailInput', () => {
    const emailAction = updateEmailInput('example@example.com');
    it('should return an object with two key value pairs, type & payload', () => {
      expect(emailAction).to.be.an('object');
      expect(emailAction).to.contain.all.keys('type', 'payload');
      expect(emailAction.type).to.deep.equal(UPDATE_EMAIL_INPUT);
      expect(emailAction.payload).to.deep.equal('example@example.com');
    });
  });

  describe('setStateToLocalStorage', () => {
    const localStorageAction = setStateToLocalStorage();
    it('should return an object with one key value pair, type', () => {
      expect(localStorageAction).to.be.an('object');
      expect(localStorageAction).to.contain.all.keys('type');
      expect(localStorageAction.type).to.deep.equal(SET_STATE_TO_LOCAL_STORAGE);
    });
  });

  describe('saveResponse', () => {
    const input1 = {
      game_key: '12345',
      num_tries_left: '4',
      phrase: 'writing some tests',
      state: 'working'
    };
    const input2 = ['a', 'b'];
    const saveAction = saveResponse(input1, input2);
    it('should return an object with two key value pairs, type & payload', () => {
      expect(saveAction).to.be.an('object');
      expect(saveAction).to.contain.all.keys('type', 'payload');
      expect(saveAction.type).to.deep.equal(SAVE_RESPONSE);
      expect(saveAction.payload.gameKey).to.deep.equal(input1.game_key);
      expect(saveAction.payload.numTriesLeft).to.deep.equal(Number(input1.num_tries_left));
      expect(saveAction.payload.phrase).to.deep.equal(input1.phrase);
      expect(saveAction.payload.state).to.deep.equal(input1.state);
      expect(saveAction.payload.played).to.deep.equal(input2);
    });
  });

  describe('changeUser', () => {
    const trueChange = changeUser(true);
    const defaultChange = changeUser();
    it('should return an object with two key value pairs, type & payload', () => {
      expect(trueChange).to.be.an('object');
      expect(defaultChange).to.be.an('object');
      expect(trueChange).to.contain.all.keys('type', 'payload');
      expect(defaultChange).to.contain.all.keys('type', 'payload');
      expect(trueChange.type).to.deep.equal(CHANGE_USER);
      expect(defaultChange.type).to.deep.equal(CHANGE_USER);
    });

    it('should return an object with a default payload of false', () => {
      /* eslint-disable no-unused-expressions*/
      expect(trueChange.payload).to.be.true;
      expect(defaultChange.payload).to.be.false;
      /* eslint-enable no-unused-expressions*/
    });
  });

  describe('removeCache', () => {
    const cacheAction = removeCache();
    it('should return an object with two key value pairs, type & payload', () => {
      expect(cacheAction).to.be.an('object');
      expect(cacheAction).to.contain.all.keys('type', 'payload');
      expect(cacheAction.type).to.deep.equal(SAVE_RESPONSE);
      expect(cacheAction.payload.gameKey).to.deep.equal(homeState.gameKey);
      expect(cacheAction.payload.numTriesLeft).to.deep.equal(Number(homeState.numTriesLeft));
      expect(cacheAction.payload.phrase).to.deep.equal(homeState.phrase);
      expect(cacheAction.payload.state).to.deep.equal(homeState.state);
      expect(cacheAction.payload.played).to.deep.equal(homeState.played);
      expect(cacheAction.payload.alphabet).to.deep.equal(homeState.alphabet);
      expect(cacheAction.payload.newUser).to.deep.equal(homeState.newUser);
    });
  });

  describe('getGameState', () => {
    const storageMock = mockStorage();
    const testString = 'state of the game';
    before(() => {
      sinon.stub(window.localStorage, 'getItem', storageMock.getItem);
      sinon.stub(window.localStorage, 'setItem', storageMock.setItem);
      localStorage.setItem('gameState', JSON.stringify(testString));
    });

    after(() => {
      window.localStorage.getItem.restore();
      window.localStorage.setItem.restore();
    });

    it(`should return an object with two key value pairs,
        type & payload with payload from localStorage`, () => {
      const getGameAction = getGameState();
      expect(getGameAction).to.be.an('object');
      expect(getGameAction).to.contain.all.keys('type', 'payload');
      expect(getGameAction.type).to.deep.equal(GET_GAME_STATE);
      expect(getGameAction.payload).to.deep.equal(testString);
    });
  });
});
