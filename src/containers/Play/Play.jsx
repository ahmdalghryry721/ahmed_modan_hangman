import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import getColor from './../../helpers/getColor';
import { actions } from './../../redux/modules/homeReducer';
import ButtonGrid from './../../components/ButtonGrid/ButtonGrid';
import MenuButton from './../../components/MenuButton/MenuButton';
import Modal from './../../components/Modal/Modal';
import LivesChip from './../../components/LivesChip/LivesChip';
import TooltippedButton from './../../components/TooltippedButton/TooltippedButton';
import FullWidthCardPanel from './../../components/FullWidthCardPanel/FullWidthCardPanel';

require('./Play.scss');

const mapStateToProps = ({ home }) => ({
  gameKey: home.gameKey,
  emailInput: home.emailInput,
  phrase: home.phrase,
  state: home.state,
  numTriesLeft: home.numTriesLeft,
  alphabet: home.alphabet,
  played: home.played,
  newUser: home.newUser
});

export class Play extends React.Component {
  static propTypes = {
    gameKey: PropTypes.string,
    emailInput: PropTypes.string,
    phrase: PropTypes.string.isRequired,
    state: PropTypes.string.isRequired,
    numTriesLeft: PropTypes.number.isRequired,
    alphabet: PropTypes.arrayOf(PropTypes.string).isRequired,
    played: PropTypes.array.isRequired,
    getGameState: PropTypes.func.isRequired,
    guessLetter: PropTypes.func.isRequired,
    setStateToLocalStorage: PropTypes.func.isRequired,
    changeUser: PropTypes.func.isRequired,
    removeCache: PropTypes.func.isRequired,
    submitEmailToCoursera: PropTypes.func.isRequired,
  }

  static contextTypes = {
    router: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);
    this.handleGuess = ::this.handleGuess;
    this.keyboardGuess = ::this.keyboardGuess;
    this.handleMouseClickGuess = ::this.handleMouseClickGuess;
    this.handleChangeUser = ::this.handleChangeUser;
    this.handleNewGame = ::this.handleNewGame;
    window.onbeforeunload = ::this.handleUnload;
  }

  componentWillMount() {
    this.props.getGameState();
    window.document.addEventListener('keydown', this.keyboardGuess);
  }

  componentDidMount() {
    $('.tooltipped').tooltip({ delay: 50 });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.newUser) {
      this.context.router.push('/');
    }
    if (nextProps.state === 'won' || nextProps.state === 'lost') {
      setTimeout(() => {
        $('#congrats').openModal();
      }, 400);
    }
  }


  componentWillUnmount() {
    this.props.setStateToLocalStorage();
    window.document.removeEventListener('keydown', this.keyboardGuess);
  }

  handleGuess(letter) {
    const { played, alphabet, guessLetter, gameKey, state } = this.props;
    if (!played.includes(letter) && alphabet.includes(letter) && state === 'alive') {
      guessLetter(letter, gameKey, played.concat([letter]));
    }
  }

  handleChangeUser() {
    this.props.removeCache();
    this.props.changeUser(true);
  }

  handleMouseClickGuess(e) {
    this.handleGuess(e.target.text);
  }

  handleNewGame() {
    this.props.submitEmailToCoursera(this.props.emailInput);
  }

  handleUnload(e) {
    e.preventDefault();
    this.props.setStateToLocalStorage();
  }

  keyboardGuess(e) {
    this.handleGuess(String.fromCharCode(e.keyCode).toLowerCase());
  }

  render() {
    const modalData = {
      header: 'Sorry :( you lost!',
      body: 'Do you want to try again?'
    };
    if (this.props.state === 'won') {
      modalData.header = 'Congratulations!!';
      modalData.body = 'You Won!!';
    }
    return (
      <div className="game-container" >
        <div className="row">
          <LivesChip numOfLives={ this.props.numTriesLeft }
            color={ getColor(this.props.numTriesLeft) }
          />
          <TooltippedButton color="blue-grey"
            phrase="Press the buttons or use a keyboard to guess the phrase"
            delay={ 50 }
            position="left"
            iconType="help"
          />
        </div>
        <div className="col s12">
          <FullWidthCardPanel content={ this.props.phrase } />
          <ButtonGrid toPlay={ this.props.alphabet }
            played={ this.props.played }
            handleClick={ this.handleMouseClickGuess }
          />
        </div>
        <MenuButton handleSwitch={ this.handleChangeUser }
          handleRenew={ this.handleNewGame }
        />
        <Modal header={ modalData.header }
          body={ modalData.body }
          handleAction={ this.handleNewGame }
          actionName={ 'Play Again' }
        />
        <br />
      </div>
    );
  }
}

export default connect(mapStateToProps, actions)(Play);
