import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { actions } from './../../redux/modules/homeReducer';

const mapStateToProps = ({ home }) => ({
  gameKey: home.gameKey,
  emailInput: home.emailInput,
  newUser: home.newUser
});

export class Home extends React.Component {
  static propTypes = {
    gameKey: PropTypes.string,
    emailInput: PropTypes.string,
    getGameState: PropTypes.func.isRequired,
    updateEmailInput: PropTypes.func.isRequired,
    submitEmailToCoursera: PropTypes.func.isRequired,
    setStateToLocalStorage: PropTypes.func.isRequired,
    changeUser: PropTypes.func.isRequired,
  }

  static contextTypes = {
    router: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);
    this.handleChange = ::this.handleChange;
    this.handleSubmit = ::this.handleSubmit;
  }

  componentWillMount() {
    this.props.getGameState();
  }


  componentWillReceiveProps(nextProps) {
    if (nextProps.gameKey && !nextProps.newUser) {
      this.context.router.push('play');
    }
  }

  componentWillUnmount() {
    this.props.setStateToLocalStorage();
  }


  handleSubmit(e) {
    const { submitEmailToCoursera, changeUser, emailInput } = this.props;
    e.preventDefault();
    changeUser();
    if (emailInput !== '') {
      submitEmailToCoursera(emailInput);
    }
  }

  handleChange({ target: { value } }) {
    this.props.updateEmailInput(value);
  }


  render() {
    return (
      <div className="row center">
        <div className="col s12 center">
          <h3 className="blue-grey-text text-darken-2">Welcome to Hangman</h3>
          <h7 className="blue-grey-text">To start playing, enter your email below!</h7>
        </div>
        <form className="col s8 offset-s1 center" onSubmit={ this.handleSubmit } >
          <div className="valign-wrapper">
            <div className="input-field col s12 offset-s2 m8 valign">
              <input type="email" id="user-email" onChange={ this.handleChange } validate />
              <label htmlFor="user-email">Email</label>
            </div>
          </div>
          <div className="col s11 offset-s2">
            <button className="btn waves-effect waves-light" type="submit">Let's Play!</button>
          </div>
        </form>


      </div>
    );
  }
}

export default connect(mapStateToProps, actions)(Home);
