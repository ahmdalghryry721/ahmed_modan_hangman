import React, { PropTypes } from 'react';

export default function LivesChip({ numOfLives, color }) {
  let lives = 'lives';
  let num = numOfLives;

  if (numOfLives === -1) {
    num = 0;
  }
  if (numOfLives === 1) {
    lives = 'life';
  }

  return (
    <div className="col">
      <div className={ `chip ${color} white-text` }>
        { `${num} ${lives} left.` }
      </div>
    </div>
  );
}

LivesChip.propTypes = {
  numOfLives: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
};
