import React, { PropTypes } from 'react';

export default function Icon({ type }) {
  return <i className="material-icons">{ type }</i>;
}

Icon.propTypes = {
  type: PropTypes.string.isRequired,
};
