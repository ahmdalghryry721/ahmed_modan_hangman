import React, { PropTypes } from 'react';
import Icon from './../Icon/Icon';

export default function TooltippedButton({ iconType, position, color, delay, phrase }) {
  return (
    <div className="col right">
      <a className={ `tooltipped btn-floating ${color}` }
        data-position={ position }
        data-delay={ delay }
        data-tooltip={ phrase }
      >
        <Icon type={ iconType } />
      </a>
    </div>
  );
}

TooltippedButton.propTypes = {
  iconType: PropTypes.string.isRequired,
  position: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  phrase: PropTypes.string.isRequired,
  delay: PropTypes.number.isRequired,
};
