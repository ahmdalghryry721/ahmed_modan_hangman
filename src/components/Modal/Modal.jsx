import React, { PropTypes } from 'react';

require('./Modal.scss');

export default function Modal({ header, body, handleAction, actionName }) {
  return (
    <div className="modal" id="congrats">
      <div className="modal-content">
        <h2 className="congrats-header blue-grey-text hide-on-small-only">
        { header }
        </h2>
        <h5 className="congrats-header blue-grey-text hide-on-med-and-up">
        { header }
        </h5>
        <p className="blue-grey-text text-darken-1 congrats-body">{ body }</p>
      </div>
      <div className="modal-footer">
        <a className="modal-action modal-close waves-effect waves-teal btn-flat"
          onClick={ handleAction }
        >
          { actionName }
        </a>
      </div>
    </div>
  );
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  handleAction: PropTypes.func.isRequired,
  actionName: PropTypes.string.isRequired,
};
