import React, { PropTypes } from 'react';

export default function CircleButton({ text, disable, handleClick, color, size }) {
  let disabled = '';
  const btnSize = size || '';
  if (disable) disabled = 'disabled';
  return (
    <a className={ `btn-floating
           ${btnSize}
           waves-effect
           waves-light
           ${color}
           ${disabled}` }
      onClick={ handleClick }
    >
      { text }
    </a>
  );
}

CircleButton.propTypes = {
  text: PropTypes.node.isRequired,
  disable: PropTypes.bool.isRequired,
  color: PropTypes.string.isRequired,
  handleClick: PropTypes.func,
  size: PropTypes.string,
};
