import React, { PropTypes } from 'react';

export default function NavBar({ title }) {
  return (
    <nav>
      <div className="nav-wrapper blue-grey">
        <a className="brand-logo center">{ title }</a>
      </div>
    </nav>
  );
}

NavBar.propTypes = {
  title: PropTypes.string.isRequired,
};
