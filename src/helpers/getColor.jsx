export default function getColor(num) {
  switch (num) {
    case 5:
      return 'green';
    case 4:
      return 'yellow darken-3';
    case 3:
      return 'amber';
    case 2:
      return 'orange';
    case 1:
      return 'deep-orange lighten-1';
    default:
      return 'red';
  }
}
