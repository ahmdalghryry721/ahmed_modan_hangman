import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import home from './modules/homeReducer';

// import all of the reducers and combine them with the following function
export default combineReducers({
  router,
  home
});
